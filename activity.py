def is_leap_year(year):
    return (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0)

def validation(year):
    while True:
        try:
            value = int(input(year))
            if value <= 0:
                print("Please enter a positive integer.")
            else:
                return value
        except ValueError:
            print("Invalid input integer.")


year = validation("Enter a year: ")

if is_leap_year(year):
    print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")


row = int(input("Input a row: "))
col = int(input("Input a col: "))

for i in range(row):
	for j in range(col):
		print("*", end = " ")
	print("")